﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ServerSideSPA2.Data;

namespace ServerSideSPA2.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly SqlDbContext _context;

        public EmployeesController(SqlDbContext  context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<List<Employee>> Get()
        {
            //    _context.Database.ExecuteSqlRaw(
            //        @"CREATE VIEW View_BlogPostCounts AS 
            //select 'Name' as Name, Departament from Employees");
            //    var x = _context.ClassQueryResults.ToList();

            
        return await _context.Employees.ToListAsync();
            //return _context.Employees
            //    .FromSqlRaw("select * from dbo.employees")
            //    .ToList();
        }

        [HttpPost("Create")]
        public async Task<bool> Create([FromBody] Employee employee)
        {
            if (ModelState.IsValid)
            {
                employee.Id = Guid.NewGuid();
                _context.Add(employee);
                try
                {
                    await _context.SaveChangesAsync();
                    return true;
                }
                catch (DbUpdateException)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        [HttpGet]
        [Route("Details/{id:Guid}")]
        public async Task<Employee> Details(Guid id)
        {
            return await _context.Employees.FindAsync(id);
        }

        [HttpPut]
        [Route("Edit/{id}")]
        public async Task<bool> Edit(Guid id, [FromBody]Employee employee)
        {
            if (id != employee.Id)
            {
                return false;
            }

            _context.Entry(employee).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return true;
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<bool> DeleteConfirmed(Guid id)
        {
            var employee = await _context.Employees.FindAsync(id);
            if (employee == null)
            {
                return false;
            }

            _context.Employees.Remove(employee);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}