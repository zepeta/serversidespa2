﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace ServerSideSPA2.Data
{
    public class AppSettingService
    {
        private readonly IConfiguration _configuration;

        public AppSettingService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string GetBaseUrl()
        {
            return _configuration.GetValue<string>("MySettings:BaseUrl");
        }
    }
}
