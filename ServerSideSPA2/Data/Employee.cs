﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ServerSideSPA2.Data
{
    public class Employee
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Departament { get; set; }
        public string Designation { get; set; }
        public string Company { get; set; }
        [NotMapped]
        public string Company1 { get; set; }
    }
}
