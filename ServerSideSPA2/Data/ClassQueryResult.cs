﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerSideSPA2.Data
{
    public class ClassQueryResult
    {
        public string Name { get; set; }
        public string Departament { get; set; }
    }
}
