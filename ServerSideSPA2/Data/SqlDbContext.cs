﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ServerSideSPA2.Data
{
    public class SqlDbContext : DbContext
    {
        public SqlDbContext(DbContextOptions<SqlDbContext> options) : base(options)
        {
            
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<ClassQueryResult> ClassQueryResults { get; set; }

        //Para mappear el resultado de una consulta
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<ClassQueryResult>(i =>
                {
                    i.HasNoKey();
                    i.ToView("View_BlogPostCounts");
                });
        }
    }
}
